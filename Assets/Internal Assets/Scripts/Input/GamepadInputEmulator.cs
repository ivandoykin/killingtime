﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamepadInputEmulator : MonoBehaviour, IInput
{
    public bool Attack()
    {
        Debug.Log("I'm emulating keyboard attack");
        return Input.GetMouseButtonDown(0);
    }

    public bool Jump()
    {
        Debug.Log("I'm emulating keyboard jump");
        return Input.GetKeyDown(KeyCode.Space);
    }

    public float Move()
    {
        return Input.GetAxis("Horizontal");
    }
}
