﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInput
{
    bool Jump();
    bool Attack();

    float Move();
}
