﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputSettings : MonoBehaviour
{
    public delegate void ApplySettings();
    public static event ApplySettings InitializeInput;

    public static IInput input { get; private set; }
    [SerializeField] private InputType inputType;

    private InputHandler[] inputHandlers;

    private enum InputType
    {
        Keyboard,
        GamepadEmulator
    }

    private void Start()
    {
        SelectInputType();
        InputInitialize();
    }

    private void SelectInputType()
    {
        switch (inputType)
        {
            case (InputType.Keyboard):
                input = GetComponent<KeyboardInput>();
                break;

            case (InputType.GamepadEmulator):
                input = GetComponent<GamepadInputEmulator>();
                break;

            default:
                Debug.LogError("Error: unknown type");
                break;
        }
    }

    private void InputInitialize()
    {
        inputHandlers = GameObject.FindObjectsOfType<InputHandler>();

        foreach (var handler in inputHandlers)
        {
            InitializeInput += handler.ApplyInputSetting;
            InitializeInput();
            InitializeInput -= handler.ApplyInputSetting;
        }
    }
}
