﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardInput : MonoBehaviour, IInput
{
    public bool Attack()
    {
        return Input.GetMouseButtonDown(0);
    }

    public bool Jump()
    {
        return Input.GetKeyDown(KeyCode.Space);
    }

    public float Move()
    {
        return Input.GetAxis("Horizontal");
    }
}
