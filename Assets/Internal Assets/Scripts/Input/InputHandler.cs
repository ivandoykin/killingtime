﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InputHandler : MonoBehaviour
{
    public IInput input;

    public abstract void ApplyInputSetting();
}
