﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameStates : MonoBehaviour
{
    public abstract void Pause();
    public abstract void Continue();
    public abstract void Win();
    public abstract void Lose();
    public abstract void Restart();
}
