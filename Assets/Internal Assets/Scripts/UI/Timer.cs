﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField] private Text winRecord;
    [HideInInspector] public Text value;

    private int seconds;
    private int minutes;

    private void Start()
    {
        value = GetComponent<Text>();
        StartCoroutine(Tik());
    }

    public void SetWinRecord()
    {
        winRecord.text = value.text;
    }

    private void UpdateTime()
    {
        seconds++;

        if (seconds == 60)
        {
            seconds = 0;
            minutes++;
        }

        if (minutes == 60)
        {
            seconds = 0;
            minutes = 0;
        }

        value.text = minutes + ":" + seconds;
    }

    private IEnumerator Tik()
    {
        yield return new WaitForSeconds(1);
        UpdateTime();
        StartCoroutine(Tik());
    }
}
