﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonstersCounter : MonoBehaviour
{
    [HideInInspector] public static MonstersCounter instance;
    [HideInInspector] public Text value;

    public static int monstersAmounts { get; private set; }

    private void Awake()
    {
        if (instance != null)
            Destroy(gameObject);

        monstersAmounts = 0;
        instance = GetComponentInParent<MonstersCounter>();
        value = GetComponent<Text>();
    }

    public static void SubscribeMonster()
    {
        monstersAmounts++;

        Debug.Log(instance == null);

        instance.value.text = monstersAmounts.ToString();
    }

    public static void UnsubscribeMonster()
    {
        monstersAmounts--;
        instance.value.text = monstersAmounts.ToString();

        if (monstersAmounts == 0)
            GameStater.instance.Win();
    }
}
