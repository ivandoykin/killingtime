﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Damagable : MonoBehaviour
{
    protected int hp = 0;

    public abstract void ApplyDamage(int value); //get damage
    public abstract void Destroing(); //if hp < 0 ---> destroy
}
