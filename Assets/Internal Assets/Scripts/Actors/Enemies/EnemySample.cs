﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySample : Damagable
{
    private void Start()
    {
        MonstersCounter.SubscribeMonster();
        hp = 3;
    }

    public override void ApplyDamage(int value)
    {
        hp += -value;

        if (hp <= 0)
            Destroing();

        Debug.Log("ouch");
    }

    public override void Destroing()
    {
        MonstersCounter.UnsubscribeMonster();
        Debug.Log("MORE GORE");
        Destroy(gameObject);
    }
}
