﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageSide : MonoBehaviour
{
    private bool attacked = false;

    private void OnTriggerStay2D(Collider2D collider)
    {
        Debug.Log(collider.GetComponent<Damagable>() != null);
        collider.gameObject.GetComponent<Damagable>().ApplyDamage(1);
        //attacked = true;
    }

    private void OnDisable()
    {
        attacked = false;
    }
}
