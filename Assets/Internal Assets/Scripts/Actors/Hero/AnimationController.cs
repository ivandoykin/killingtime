﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : InputHandler
{
    public delegate void AnimationInput();

    private const string anim_damaged = "damaged";
    private const string anim_grounded = "grounded";
    private const string anim_death = "death";
    private const string anim_run = "run";
    private const string anim_jump = "jump";
    private const string anim_attack = "attack";
    private const string anim_deathEnd = "deathEnd";

    private Animator animator;

    public override void ApplyInputSetting()
    {
        input = InputSettings.input;
    }

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void Attack()
    {
        animator.SetBool(anim_attack, true);
        StartCoroutine(Wait(anim_attack));
    }

    public void Fall() => animator.SetBool(anim_grounded, false);

    public void Grounding() => animator.SetBool(anim_grounded, true);

    public void Death() 
    {
        animator.SetBool(anim_death, true);
        StartCoroutine(Wait(anim_deathEnd, 0.5f));
    }

    public void Jump() 
    {
        animator.SetBool(anim_jump, true);
        StartCoroutine(Wait(anim_jump));
    }

    public void Run() => animator.SetBool(anim_run, true);

    public void StopRunning() => animator.SetBool(anim_run, false);

    public void Damaged()
    {
        Debug.Log("Damage!");
        animator.SetBool(anim_damaged, true);
        StartCoroutine(Wait(anim_damaged));
    }

    private IEnumerator Wait(string animation, float time = 0.01f)
    {
        yield return new WaitForSeconds(time);
        animator.SetBool(animation, false);
    }
}
