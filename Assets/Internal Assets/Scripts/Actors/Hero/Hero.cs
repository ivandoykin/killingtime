﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : Damagable
{
    private AnimationController animationController;
    public static event AnimationController.AnimationInput StateAnimations;

    private void Start()
    {
        hp = 3;

        animationController = GetComponent<AnimationController>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            ApplyDamage(3);
            Debug.Log(Time.timeScale);
        }
    }

    public override void ApplyDamage(int value)
    {
        hp += -value;

        StateAnimations += animationController.Damaged;
        StateAnimations();
        StateAnimations -= animationController.Damaged;

        if (hp <= 0)
            Destroing();
    }

    public override void Destroing()
    {
        StateAnimations += animationController.Death;
        StateAnimations();
        StateAnimations -= animationController.Death;

        //some Die anim
        //GameStater.instance.Lose();
    }
}
