﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroInput : InputHandler
{
    private const float attackDuration = 0.40f;

    public static event AnimationController.AnimationInput AnimationHandler;

    public bool isGrounded = false;

    [SerializeField] private int speed = 6;
    [SerializeField] private int jumpForce = 12;

    [SerializeField] private GameObject LeftAttackSide;
    [SerializeField] private GameObject RightAttackSide;
    [SerializeField] private float attackCooldownTime = 0.25f;
    private float currentCooldownTime = 0f;

    private Rigidbody2D body;
    private SpriteRenderer sprite;

    private AnimationController animationController;

    private void Start()
    {
        animationController = GetComponent<AnimationController>();

        body = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
    }

    public override void ApplyInputSetting()
    {
        input = InputSettings.input;
    }

    private void Update()
    {
        if (Time.timeScale == 0)
            return;

        currentCooldownTime += Time.deltaTime;

        CheckGround();
        Attack();
        Jump();
        Move();
    }

    private void CheckGround()
    {
        if (isGrounded)
        {
            AnimationHandler += animationController.Grounding;
            AnimationHandler();
            AnimationHandler -= animationController.Grounding;
        }

        else
        {
            AnimationHandler += animationController.Fall;
            AnimationHandler();
            AnimationHandler -= animationController.Fall;
        }
    }

    private void Attack()
    {
        bool isAttack = input.Attack();

        if (isAttack && currentCooldownTime > attackCooldownTime)
        {
            Debug.Log("Attack!");
            currentCooldownTime = 0f;

            AnimationHandler += animationController.Attack;
            AnimationHandler();
            AnimationHandler -= animationController.Attack;

            if (sprite.flipX == false)
                StartCoroutine(Attacking(RightAttackSide, attackDuration));
            else
                StartCoroutine(Attacking(LeftAttackSide, attackDuration));
            //something
        }
    }

    private IEnumerator Attacking(GameObject side, float time)
    {
        side.SetActive(true);
        yield return new WaitForSeconds(time);
        side.SetActive(false);
    }

    private void Jump()
    {
        bool isJumped = input.Jump();

        if (isJumped && isGrounded)
        {
            isGrounded = false;
            body.velocity = new Vector2(body.velocity.x, jumpForce);
        }
    }

    private void Move()
    {
        float moveX = input.Move();

        if (moveX > 0)
            sprite.flipX = false;
        else if (moveX < 0)
            sprite.flipX = true;

        body.velocity = new Vector2(moveX * speed, body.velocity.y);

        if (Mathf.Abs(moveX) > 0.1f)
        {
            AnimationHandler += animationController.Run;
            AnimationHandler();
            AnimationHandler -= animationController.Run;
        }

        else
        {
            AnimationHandler += animationController.StopRunning;
            AnimationHandler();
            AnimationHandler -= animationController.StopRunning;
        }
    }
}
