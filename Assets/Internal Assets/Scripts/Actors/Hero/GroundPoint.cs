﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]

public class GroundPoint : MonoBehaviour
{
    private HeroInput hero;

    private void Start()
    {
        hero = GetComponentInParent<HeroInput>();
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<Floor>() != null)
        {
            hero.isGrounded = true;
        }
    } 

    private void OnCollisionExit2D(Collision2D collision)
    {
        hero.isGrounded = false;
    }
}
