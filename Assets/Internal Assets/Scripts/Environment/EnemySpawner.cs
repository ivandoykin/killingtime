﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    private const int luck = 10;

    [SerializeField] private GameObject enemy;
    [SerializeField] private Transform spawnPoint;

    private void Start()
    {
        if (Random.Range(0, 100) >= luck)
            Instantiate(enemy, spawnPoint.position, new Quaternion(0,0,0,0));
    }
}
