﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStater : GameStates
{
    public delegate void WinEvents();
    public static event WinEvents Winning;

    [SerializeField] private Timer timer;
    [SerializeField] private GameObject pausePanel;
    [SerializeField] private GameObject winPanel;

    public static GameStater instance;
    public static bool paused = false;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        else
        {
            Debug.LogError("Try get access to GameStater");
            Destroy(gameObject);
        }

    }

    public override void Continue()
    {
        pausePanel.SetActive(false);

        Time.timeScale = 1;
        paused = false;
    }

    public override void Lose()
    {
        //u NOT Lose in this game :)
    }

    public override void Pause()
    {
        pausePanel.SetActive(true);

        Time.timeScale = 0;
        paused = true;
    }

    public override void Win()
    {
        Winning += timer.SetWinRecord;
        Winning();
        Winning -= timer.SetWinRecord;

        winPanel.SetActive(true);

        Time.timeScale = 0;
        paused = true;
    }

    public override void Restart()
    {
        Time.timeScale = 1;

        Scene scene = SceneManager.GetActiveScene(); 
        SceneManager.LoadScene(scene.name);
    }
}
